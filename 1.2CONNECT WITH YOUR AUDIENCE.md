# Connect with Your Audience

鑑賞者とつながる

IF YOU REMEMBER ONLY ONE THING FROM THIS ENTIRE BOOK, remember to keep your audience in mind while you’re writing.

もしこの本全体の中で一つのことしか覚えられないとしたら、文章を書いているときは鑑賞者のことを気に留めておかなければならない、ということを覚えておいてください。

This is more important than you may realize.

これはあなたが考えているよりも重要なことなのです。

## Who is your audience?
あなたの鑑賞者はだれ？

**purcahse**?

	Your audience is anyone who might buy your art, especially if they understood more about it.
	“To buy” means “to purchase.”
	“To buy” also means “to accept, believe, or to take seriously.”
	
--

	あなたにとっての鑑賞者とは、作品を買ってくれそうな誰かのこと、特に作品をより理解することで買ってくれそうな人のことです。そして”買う”とは”取得する”ことであり、”認めて、信用し、真剣に受け止める”ということなのです。

Pay attention to the definition of audience in this context; the definition is limited for good reason.

**以上の文脈に基づいて**鑑賞者が定義されていることに注意してください。鑑賞者の定義はまっとうな理由によって決められているのです。

If you like to write, and the audience I describe feels too confining, then find other outlets for your writing. If you want to discuss art theory and politics, communicate with like-minded thinkers, or shred your enemies, go to online forums. Write your rants and manifestos in a blog or journal. Find a place to write in unrestrained prose.

もし文章を書くことが好きだからこそ、私が説明したような鑑賞者の定義がとても窮屈に感じるようでしたら、文章の発表する別のはけ口を見つけておくべきでしょう。美術の理論や政治性について議論を交わしたいのならば意を共にする仲間たち、またはライバルたちとおこないましょう。オンライン上のフォーラムに行くのも良いですし、あなたの熱弁と宣言をブログや日記に書くのもありです。気ままに散文を書ける場をみつけておくのです。

Want a brilliant example? Artist Ai Weiwei’s blog is now published as a book, Ai Weiwei’s Blog: Writings, Interviews and Digital Rants, by Ai Weiwei and Lee Ambrozy.

魅力的な事例を知りたいですか？アーティストであるアイ・ウェイウェイのブログに記された文章、インタビュー、熱い言葉の数々は、現在アイ・ウェイウェイとリー・アンブロージーによって本として出版されています。

The more you write, the easier the words will come when it’s time to describe your work. But when you’re writing your artist statement, you’ll need to reign in your self-expression.

1. reign 統治する
2. self-expression = express myself

たくさん書いていれば、あなた自身の作品について書く際にも言葉が次々と降ってくることでしょう。しかしあなたが書いているのはアーティスト・ステイトメントなので、自己表現を制御する必要があるのです。

To present yourself professionally, concisely, and effectively, address the people who might “buy” your work. It’s easy to get off track if you write without holding on to this definition. All writing that represents you in the light of self-promotion needs to be directed at a defined and well-considered audience. Whoever reads your statement is attempting to understand your artwork. Respect their attempt to learn about you, and you’ll improve your chances of connecting with them.

あなた自身を本格的に、簡潔かつ効果的に売り込むには、あなたの作品を"買う"かもしれない人々にことばを向けることです。この定義を掴んでいなければ、文章はすぐに軌道から外れていってしまいます。全ての文章はセルフ・プロモーションとしてあなたを表現し、なおかつ熟考の上で定義された鑑賞者に向けて書かれている必要があります。あなたのステイトメントを読む人は、誰もがあなたの作品を理解したいのです。あなたについて知りたいという人達の思いを尊重しましょう。そして彼らとつながるチャンスを増やしましょう。


Remember this key phrase in the definition of an artist statement:

アーティスト・ステイトメントの定義におけるこのキーフレーズを覚えておいてください。

*An artist statement is a concise arrangement of words that acts as a bridge to* **connect your audience** *to your art.*

アーティスト・ステイトメントとは、鑑賞者と作品をつなぐ橋となるように言葉を簡潔に整理することである。

Let’s examine who your audience is NOT. You are not writing to answer critics, impress your ex-professor or former girlfriend, or amuse yourself and your friends. 

誰があなたの鑑賞者ではないのかを考えてみましょう。あなたは批評家のお眼鏡にかなうため、恩師や元カノを感動させるため、自分自身や友人を楽しませるために書いているのではありません。

Here are a few more ways to misfire:

これらは失敗を招くやり方といえます。：

### You are NOT talking to your therapist

あなたは自分のセラピストに向けて話しているのではありません

Somewhere along the way, you may have picked up the notion that writing about your art means communicating all that’s going on inside your head. You don’t have to do that! In fact, please don’t! Say one thing clearly. Then say another thing clearly. When it starts sounding muddled or self-obsessive, stop writing.

1. muddled 混乱している、曖昧である
2. obsessive 強迫的な状況にある

このやり方に沿っていると、自分の作品について書くということは脳内のできごと全てを伝えることなのだ、という誤った認識を持ってしまうかもしれません。でもそんなことをする必要はありません！というより、どうかやらないでください！ひとつずつ、明確にものごとを伝えていきましょう。言っていることがごちゃごちゃになってきたり、自意識過剰な文章になり始めたときは書くのを中断してください。

The act of writing about your work will often help you understand your process, clarify ideas, and reveal new insights. But writing your artist statement is not about voicing your every thought. Your inner monologue is a confusing mess of contradictions, a struggle between self-doubt and inflated ego. Of course it may include all kinds of things that you think are fascinating, but probably only your therapist or maybe a patient loved one will agree; anyone other than that will find your inner monologue tiresome. As the child psychiatrist said to his chatty daughter, **“Honey, some of your ideas can just be thoughts. ”**

1. clarify 明確にするVt
2. reveal (隠れていたものを)見せる、公開する、明らかにする
3. contradiction 矛盾、対立
4. tiresome うんざりする、退屈だ

作品について書くことは、ときに自分の手法を理解することやアイデアを明確にすること、新しい考え方を発見することの手助けにもなります。しかし、アーティスト・ステイトメントを書くことは考えていること全てをことばにすることではないのです。あなたの内的な独白は、自己不信と膨張したエゴの狭間で起きる葛藤によって、矛盾とわかりにくさに満ちた混乱を招く文章となることを理解してください。もちろん、そのような文章にはあなたが魅力的だと考えていることも全て書かれているのかもしれませんが、おそらく賛同してくれるのはセラピストかあなたに夢中な人だけでしょう。その他大勢にとって、あなたの内なる独白など退屈なだけなのです。たとえば、ある小児精神科医がおしゃべりな我が子にこう言ったように。「ハニー、きみのそのアイデアってただの思いつきじゃないかな。」

Your task is to communicate your insights and ideas to an audience in a way that they might find interesting. 

自分の見識やアイデアを鑑賞者へ伝えるのがあなたの課題です。**それを実現するための方法の一つが、彼らが興味を持ってくれそうなことを書くことです。**

## You are NOT writing to argue with a person, group, or ideology 

**あなたは個人、集団、イデオロギーとの討論を書くのでありません**

If your art is political, or **emotionally charged**, explain why the issue moves you instead of why the issue should move the audience. 
Avoid telling other people how they should feel.

感情的な文章を書くわけでもありません。なぜそれが鑑賞者を感動させるはずなのかを書く代わりに、なぜそれがあなたを感動させたのかを書きましょう。他者に対してこう感じるべきだ、などと言うことは止めましょう。
	
	You must respect your audience enough to let them feel what they feel and think what they think in the presence of your work.

**There is a difference between being angry and being argumentative.** Your audience is reading your statement to learn what you’re about, not to engage in **some assumed intellectual conflict**.

1. argumentative 理屈っぽい、議論が好きな

鑑賞者を充分に尊重し、あなたの作品と対面することで彼らなりの感じ方と考えを持てるようにしましょう。あなたの鑑賞者はあなたについて知りたくてステイトメントを読んでいるのです。知的な対立を想定して読んでいるのではありません。

An argumentative tone in your writing will just turn the audience away. Let your art confront and argue your point; let your writing present your message in plain language, or merely introduce the topic of your work.

議論めいた内容の文章は鑑賞者を遠ざけるだけです。**自分の作品を見つめて、作品の要点に付いて話してください。**文章はあなたのメッセージを平易なことばで表現すると共に、あなたの作品に関するトピックだけを紹介するべきでしょう。

## You are NOT writing to impress other artists or art experts

あなたは他のアーティストやアートの専門家に自分をアピールする為に文章を書いているのではありません。

Reduce the artspeak in your statement to a minimum. By “artspeak,” I’m referring to the c of visual artists. These specialist terms, so familiar to you, can make non-artists feel uninformed and unconnected. Artspeak tends to describe art in general, yet fails to communicate what is singular and distinctive about your art.

ステイトメントからは美術用語を極力減らしましょう。"アートスピーク"は視覚芸術に携わる作家の方言の様なものなのですから。((※アートスピークという表現は、ジョージ・オーウェルの『1984』に出てくる“ニュースピーク”(全体主義によって支配されている、退廃した言語体系)のもじりだと思われます。)) 専門家が使うようなことばのいくつかは、あなたにとっても親しみ深いものでしょう。ですがアーティストではない人々にとってはそんなこと知らないし関係ない、と感じさせてしまう可能性があります。更に美術用語は通常、あなたの作品の何が独特なのか伝えることを失敗させてしまいかねないものなのです。

Of course you’ll use art vocabulary to describe your work.You’ll have to judge for yourself if your chosen art terms genuinely help to get your point across, or if you’re just stretching out sentences with echoed phrases. You run the risk of distancing your reader and dulling your individuality when you overuse artspeak. 

1. ggenuinely 本当に
1. get O across Oが理解される
1. point 主張、要点、論点
1. echo 反響するVi、人の意見に同調するVt、人の発言をオウム返しするVt 
1. distance 遠ざけるVt
1. dull 鈍くさせるVt、鈍くなるVi
1. run the risk of の危険を冒す
1. indivisuality 個性
1. overuse 使いすぎるVt

もちろんあなたは自分の作品について説明するために美術に関することばの数々を使うことになります。その際にあなたは、自分の選んだ美術用語が本当に作品の本質を理解する助けになっているのか、もしくはただフレーズを真似することで文章を長くしているだけなのかを自分自身で判断しなければなりません。美術用語を過度の使用する際には、あなたの個性を曇らせ、読者を遠ざけてしまうリスクをすり抜けながら進んでいくことになります。

The exception: If you’re reading this book while in art school, you have a specialized audience of artists and academics. You probably use lots of artspeak, which is appropriate for your environment. You’re currently surrounded, as you may never be again, by people who communicate using art terminology. Ask your teachers what they think about the following list of bulleted questions. 

1. You’re currently surrounded, as you may never be again, by people who communicate using art terminology.
1. You’re currently surrounded by people who communicate using art terminology.
1. as you may never be again surrounded by -----


ただし例外として、この本を読んでいるあなたが美大に在席中ならば、アカデミックな人々やアーティストなどの専門性の高い鑑賞者に恵まれるでしょう。そんなあなたはおそらく、自分の状況に最適な美術用語を沢山使っているはずです。周囲でもみなさんが専門用語で話していることでしょうけれど、あなたは二度と使用することはないでしょう。あなたの先生達へ、以下に箇条書きされた質問に対してどのように考えているか訪ねてみましょう。

Examine your writing and ask yourself:

そしてあなたの文章を精査し、自分自身にも問いかけてみてください。

- Do these words truly help to explain what people are seeing?
- Is there a sentence I could translate from artspeak into plain language?
- Is this the truth?
- Do I fully understand what I just wrote? 
- What words might create a better understanding for the reader? 


・この言葉は本当に鑑賞対象を説明することに役立っていますか？
・もっと簡単なことばに翻訳できる美術用語はありませんか？
・本当のことを書いていますか？
・書いた内容を自分ですべて理解できていますか？
・読者により深い理解を促すであろう言葉はなんでしょうか？

Simple consideration for your reader goes a long way toward adopting a tone of directness and truth.

1. go a long way 長持ちする、大きな役割を果たす
1. adopt ここでは身に付ける、か。
1. consideration 検討項目、熟考 

これらは単純な検討項目ですが、読者のためにわかりやすく真摯な文を書くための道のりは長く険しいのです。

Your readers/viewers will perceive your attempt to connect with them through the written word. Connecting your audience to your art is important — very important. Because, in the end, they might just **buy it.**

読者・鑑賞者は 、書かれている言葉を通してあなたが自分たちとつながろうとしていることに気づきます。鑑賞者とあなたの作品がつながることは重要です ― めちゃんこ重要なのです。なぜなら、最終的に彼らはそれを買ってくれるかもしれないのですから。

	Your audience is anyone who might buy your art, especially if they understood more about it. “To buy” means “to purchase.” “To buy” also means “to accept, believe, or to take seriously.”
	
--

	あなたにとっての鑑賞者とは作品を買ってくれるかもしれない誰かのことです。“買う”とは“取得する”ことであり、“認めて、信用し、真剣に受け止める”ことでもあるのです。
