## Preface: Words Are Powerful

序文：言葉は強力である

### LANGUAGE AND IMAGE CONNECT IN MYSTERIOUS WAYS.

言葉と視覚表現は神秘的な結びつきかたをしている

I first experienced this connection as an art history student. In darkened lecture halls, gazing at detailed slides of art I’d never seen before and listening to a flow of descriptive words, I entered a state of true amazement. To the credit of my professors, at the end of each class I not only knew so much more, but I could see so much more. Somehow, words combined with images had changed my brain. These art history lectures actually increased my ability to see.

私が初めてこの結びつきを経験したのは、まだ美術史の学生だったころでした。暗い講義室で、今まで見たことのない詳細なスライドを凝視し、溢れんばかりの専門用語に耳を傾け、驚くことがたくさんありました。教授のおかげで、最終的には様々な知識を得ただけではなく、作品をより詳細に鑑賞することができるようになりました。イメージと言葉が結びつき、私の脳が変化したのです。この授業は私の作品を見る能力を向上させてくれました。

I was very fortunate to have Rudolph Arnheim as a professor. His classic text books, Art and Visual Perception and Visual Thinking, are dense with theory, but I can tell you, in person, the man himself always seemed joyous — as if nothing could be better than speaking to this group of students about this subject at this moment in time.

Rudolph Arnheim 教授に師事できたのは幸運でした。先生の素晴らしい教科書 [Art and Visual Perception and Visual Thinking](https://www.amazon.co.jp/Art-Visual-Perception-Psychology-Creative/dp/0520243838/ref=sr_1_1?s=english-books&ie=UTF8&qid=1469058072&sr=1-1) には美術の理論がたくさんつめ込まれていますが、私が直接受けた印象は少し違っていて、先生はいつも楽しそうな人でした。目の前の学生に話している、この瞬間こそが、何よりも価値のあるものだと感じているようだったのです。

Before you delve into Art-Write, I’d like to simplify and share some of what I learned from Dr. Arnheim. His insights illuminate the path to writing about your art.

さて、「Art-Write」について探求する前に、まずはアーンハイム博士から私が教わったことを整理し皆さんに共有します。先生の知見はきっと、皆さんが芸術について文章を書くための道筋を示してくれるでしょう。

### The Arnheim Insights
- The act of “seeing” or “looking” is not a simple process
- Seeing takes time to accomplish
- Just as an artist toils with his or her own powers of perception and sense of vision to create a work of art, the viewer toils with his or her own powers of perception and sense of vision to “see” a work of art
- Language shapes perception

--

- 「見る」もしくは「鑑賞する」という行為は決して単純な工程ではない
- 「見る」ことを習得するには、相応の時間が必要になる
- アーティストが自分の知覚する力と視覚を総動員して作品をつくるように、鑑賞者は自分の知覚する力と視覚をすべて使って作品を「見る」
- 言葉が知覚を方向づける

When you consider these useful ideas, you can begin to understand how your audience approaches your artwork. Truly “seeing” art is an accomplishment. It doesn’t happen quickly. Your viewers need time and they may need some education. Language can help educate them.

これらの知見をよくよく考慮していきますと、どのように観衆があなたの作品に対自していくのかということが理解できるかもしれません。本当の意味で芸術を「鑑賞する」というのは、ある種の偉業なのです。すぐにできるようにはなりません。あなたの作品の鑑賞者はそのための時間が必要だし、教育も必要になります。言葉はそのための手助けになります。

You, the artist, talking and writing about your work, are in a position to provide language that shapes perception. Words are powerful, and you can use them to help your audience see.

芸術家であるあなたは、自分自身の作品について話したり書いたりするわけですが、それによって知覚を方向づけるための言葉を提供する立場にあります。言葉は強力ですから、用いることによって、観衆があなたの作品を理解する助けをすることができるのです。
